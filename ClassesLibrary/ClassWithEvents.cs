﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesLibrary
{
    public class ClassWithEvents
    {
        #region Event without argument
        
        // Either (1)
        public event EventHandler ThresholdReached;

        // Or the two following lines (2)
        /*
        public delegate void ThresholdReachedEventHandler(object sender, EventArgs e);
        public event ThresholdReachedEventHandler ThresholdReached;
        */
        public virtual void OnThresholdReached(EventArgs e)
        {
            //(1)
            EventHandler handler = ThresholdReached;
            
            //(2)
            //ThresholdReachedEventHandler handler = ThresholdReached;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        #endregion

        #region Event with arguments
        // Either (1)
        public event EventHandler<ThresholdReachedEventArgs> ThresholdReachedWithArgs;

        // Or the two following lines (2)
        /*
        public delegate void ThresholdReachedEventHandlerWithArgs(object sender, EventArgs e);
        public event ThresholdReachedEventHandlerWithArgs ThresholdReachedWithArgs;
        */
        public virtual void OnThresholdReachedWithArgs(ThresholdReachedEventArgs e)
        {
            //(1)
            EventHandler<ThresholdReachedEventArgs> handler = ThresholdReachedWithArgs;
            
            //(2)
            //ThresholdReachedEventHandlerWithArgs handler = ThresholdReachedWithArgs;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        #endregion

        #region Custom Event Accessors

        #region Without arguments
        private event EventHandler ehEvenementWithoutArgs = delegate { };
        public event EventHandler EhEvenementWithoutArgs
        {
            add
            {
                ehEvenementWithoutArgs += value;
            }

            remove
            {
                ehEvenementWithoutArgs -= value;
            }
        }

        public void DoEvenementWithoutArgs()
        {
            EventHandler handler = ehEvenementWithoutArgs;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        #endregion

        #region With arguments
        private event EventHandler<ThresholdReachedEventArgs> ehEvenementWithArgs = delegate { };
        public event EventHandler<ThresholdReachedEventArgs> EhEvenementWithArgs
        {
            add
            {
                ehEvenementWithArgs += value;
            }

            remove
            {
                ehEvenementWithArgs -= value;
            }
        }

        public void DoEvenementWithArgs(ThresholdReachedEventArgs e)
        {
            EventHandler<ThresholdReachedEventArgs> handler = ehEvenementWithArgs;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        #endregion
        #endregion
    }
}
