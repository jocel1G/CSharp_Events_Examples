﻿using ClassesLibrary;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application_for_Events
{
    class Program
    {
        #region Event without argument
        private static void fctWithoutParameter (object sender, EventArgs e)
        {
            Console.WriteLine("In fctWithoutParameter");
        }
        #endregion

        #region Event with arguments
        private static void fctWithParameters(object sender, ThresholdReachedEventArgs e)
        {
            Console.WriteLine("In fctWithParameters");
            Console.WriteLine(e.TimeReached.ToString());
        }
        #endregion

        #region Custom Event Accessors
        #region Without arguments
        private static void fctForEvenementWithoutArgs (object sender, EventArgs e)
        {
            Console.WriteLine("In fctForEvenementWithoutArgs");
        }
        #endregion

        #region With arguments
        private static void fctForEvenementWithArgs(object sender, ThresholdReachedEventArgs e)
        {
            Console.WriteLine("In fctForEvenementWithArgs");
            Console.WriteLine(e.Threshold.ToString() + ", " + e.TimeReached.ToString());
        }
        #endregion
        #endregion

        static void Main(string[] args)
        {
            ClassWithEvents clsWithEvent = new ClassWithEvents();
            #region Event without argument
            clsWithEvent.ThresholdReached += fctWithoutParameter;

            clsWithEvent.OnThresholdReached(EventArgs.Empty);
            #endregion

            #region Event with arguments
            ThresholdReachedEventArgs trea = new ThresholdReachedEventArgs() { Threshold = 2, TimeReached = new DateTime(2001, 12, 23) };
            // Either
            clsWithEvent.ThresholdReachedWithArgs += fctWithParameters;
            // Or
            //clsWithEvent.ThresholdReachedWithArgs += (sender, e) => fctWithParameters(sender, e);

            clsWithEvent.OnThresholdReachedWithArgs(trea);

            Action<object, ThresholdReachedEventArgs> actTREA = delegate (object sender, ThresholdReachedEventArgs e)
            {
                Console.WriteLine("In actTREA" + e.Threshold.ToString());
            };

            clsWithEvent.ThresholdReachedWithArgs += new EventHandler<ThresholdReachedEventArgs>(actTREA);

            clsWithEvent.OnThresholdReachedWithArgs(trea);

            #endregion

            #region Custom Event Accessors
            #region Without arguments
            clsWithEvent.EhEvenementWithoutArgs += fctForEvenementWithoutArgs;
            clsWithEvent.DoEvenementWithoutArgs();
            clsWithEvent.EhEvenementWithoutArgs -= fctForEvenementWithoutArgs;
            clsWithEvent.DoEvenementWithoutArgs();
            #endregion
            #region With arguments
            ThresholdReachedEventArgs trea_CEA = new ThresholdReachedEventArgs() { Threshold = 63, TimeReached = new DateTime(2011, 10, 3) };
            clsWithEvent.EhEvenementWithArgs += fctForEvenementWithArgs;
            clsWithEvent.DoEvenementWithArgs(trea_CEA);
            clsWithEvent.EhEvenementWithArgs -= fctForEvenementWithArgs;
            clsWithEvent.DoEvenementWithArgs(trea_CEA);
            #endregion
            #endregion
            Console.Read();
        }
    }
}
